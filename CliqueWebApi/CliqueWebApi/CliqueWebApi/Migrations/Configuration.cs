namespace CliqueWebApi.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CliqueWebApi.Infrastructure.ApplicationDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CliqueWebApi.Infrastructure.ApplicationDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            var manager = new UserManager<AccountInformation>(new UserStore<AccountInformation>(new Infrastructure.ApplicationDBContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new Infrastructure.ApplicationDBContext()));

            var user = new AccountInformation()
            {
                UserName = "somya.gupta@hotmail.ca",
                Email = "somya.gupta@hotmail.ca",
                EmailConfirmed = true,
                FirstName = "Somya",
                LastName = "Gupta",
                CreatedOn = DateTime.Now.AddYears(-3),
                LastLogin = DateTime.Now.Date,
                CurrentlyLogin = false,
                AccessDeniedCount = 0
            };

            manager.Create(user, "Netsecure18!");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole { Name = "SuperAdmin" });
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }

            var adminUser = manager.FindByName("somya.gupta@hotmail.ca");

            manager.AddToRoles(adminUser.Id, new string[] { "SuperAdmin", "Admin" });
        }
    }
}
