﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CliqueWebApi.Infrastructure
{
    public class ApplicationDBContext: IdentityDbContext<AccountInformation>
    {
        public ApplicationDBContext() :base("AccountInformation", throwIfV1Schema:false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static ApplicationDBContext Create()
        {
            return new ApplicationDBContext();
        }
    }
}