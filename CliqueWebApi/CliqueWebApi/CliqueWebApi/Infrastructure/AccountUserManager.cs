﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CliqueWebApi.Infrastructure
{
    public class AccountUserManager: UserManager<AccountInformation>
    {
        public AccountUserManager (IUserStore<AccountInformation> store):base(store)
        {

        }


        public static AccountUserManager Create(IdentityFactoryOptions<AccountUserManager> options, IOwinContext context)
        {
            var appDBContext = context.Get<ApplicationDBContext>();
            var appUserManager = new AccountUserManager(new UserStore<AccountInformation>(appDBContext));

            //Configure validation logic for passwords
            appUserManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = false,
                RequireLowercase = true,
                RequireUppercase = true
            };

            //Configure validation logic for User Email
            appUserManager.UserValidator = new UserValidator<AccountInformation>(appUserManager)
            {
                RequireUniqueEmail = true
            };

            appUserManager.EmailService = new CliqueWebApi.Services.EmailServices();
            var dataProtectionProvider = options.DataProtectionProvider;

            if(dataProtectionProvider != null)
            {
                appUserManager.UserTokenProvider = new DataProtectorTokenProvider<AccountInformation>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    //Code for email confirmation and reset password life time
                    TokenLifespan = TimeSpan.FromHours(24)
                };
            }

            return appUserManager;
        }
    }
}