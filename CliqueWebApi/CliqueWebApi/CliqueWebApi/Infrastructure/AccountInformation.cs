namespace CliqueWebApi
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public partial class AccountInformation : IdentityUser
    {
 
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        
        [Column(TypeName = "date")]
        public DateTime CreatedOn { get; set; }

        [Column(TypeName = "date")]
        public DateTime LastLogin { get; set; }

        public bool CurrentlyLogin { get; set; }

        public int AccessDeniedCount { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AccountInformation> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }



}
